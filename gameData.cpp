#include "gameData.h"
#include <cmath>
#include <iostream>
#include <limits>
#include <sstream>

Gamedata& Gamedata::getInstance() {
    static Gamedata gamedata;
    return gamedata;
}

Gamedata::~Gamedata() {}

Gamedata::Gamedata(const string& fn)
    : parser(fn), gameData(parser.getXmlData()) {}

double Gamedata::getRandInRange(int min, int max) const {
    return min +
           (rand() / (std::numeric_limits<int>::max() + 1.0f)) * (max - min);
}

double Gamedata::getRandFloat(double min, double max) const {
    return min + (rand() / (RAND_MAX + 1.0f)) * (max - min);
}

bool Gamedata::checkTag(const std::string& tag) const {
    return gameData.count(tag) != 0;
}

bool Gamedata::getXmlBool(const string& tag) const {
    std::map<string, string>::const_iterator ptr = gameData.find(tag);
    if (ptr == gameData.end())
        throw string("Game: Didn't find boolean tag ") + tag +
            string(" in xml");
    else {
        if (ptr->second == "true")
            return true;
        else
            return false;
    }
}

int Gamedata::getXmlInt(const string& tag) const {
    std::map<string, string>::const_iterator ptr = gameData.find(tag);
    if (ptr == gameData.end())
        throw string("Game: Didn't find integer tag ") + tag +
            string(" in xml");
    else {
        std::stringstream strm;
        strm << ptr->second;
        std::string stringNumber = strm.str();
        int data = 0;
        strm >> data;
        if (strm.fail()) {
            throw "Invalid integer for " + tag + string(" in xml");
        }
        return data;
    }
}

double Gamedata::getXmlFloat(const string& tag) const {
    std::map<string, string>::const_iterator ptr = gameData.find(tag);
    if (ptr == gameData.end())
        throw string("Game: Didn't find double tag ") + tag + string(" in xml");
    else {
        std::stringstream strm;
        strm << ptr->second;
        double data = 0.0f;
        strm >> data;
        if (strm.fail()) {
            throw "Invalid double for " + tag + string(" in xml");
        }
        return data;
    }
}

const string& Gamedata::getXmlStr(const string& tag) const {
    std::map<string, string>::const_iterator ptr = gameData.find(tag);
    if (ptr == gameData.end())
        throw string("Game: Didn't find string tag ") + tag + string(" in xml");
    else
        return ptr->second;
}

void Gamedata::displayData() const {
    for (auto ptr = gameData.cbegin(); ptr != gameData.cend(); ptr++) {
        std::cout << ptr->first << ", " << ptr->second << std::endl;
    }
}
