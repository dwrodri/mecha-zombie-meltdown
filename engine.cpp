#include "engine.h"
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <random>
#include <sstream>
#include <string>
#include "frameGenerator.h"
#include "gameData.h"
#include "mainMenu.h"
#include "multisprite.h"
#include "player.h"
#include "spotlight.h"
#include "sprite.h"
#include "twoWayMultiSprite.h"
#include "zombie.h"

Engine::~Engine() { std::cout << "Terminating program" << std::endl; }

Engine::Engine()
    : rc(RenderContext::getInstance()),
      io(IoMod::getInstance()),
      clock(Clock::getInstance()),
      collisionChecker(CollisionChecker::getInstance()),
      playerLocObserver(PlayerLocObserver::getInstance()),
      zombieManager(ZombieManager::getInstance()),
      bulletManager(BulletManager::getInstance()),
      sdlSound(SDLSound::getInstance()),
      spotlight(Spotlight::getInstance()),
      renderer(rc.getRenderer()),
      back("back", Gamedata::getInstance().getXmlInt("back/factor")),
      hud(Hud::getInstance()),
      mainMenu(MainMenu::getInstance()),
      viewport(Viewport::getInstance()),
      sprites(),
      player(nullptr),
      currentSprite(0),
      wavesDefeated(0),
      makeVideo(false),
      inMenu(true),
      paused(true) {
    hud.toggleVisibility();
    sprites.reserve(65);
    sprites.emplace_back(new Player("player"));
    player = dynamic_cast<Player*>(sprites[0]);
    player->attach(&playerLocObserver);
    zombieManager.getNextWave(sprites);
    std::for_each(sprites.begin() + 1, sprites.end(),
                  [](auto& x) { dynamic_cast<Zombie*>(x)->randomSpawn(); });
    Viewport::getInstance().setObjectToTrack(sprites[0]);
    std::cout << "Construction of Engine complete" << std::endl;
    mainMenu.setTitleText("Mecha Zombie Meltdown");
    mainMenu.setSubtitleText("press space to begin or q to quit");
}

void Engine::draw() const {
    if (!paused) {
        back.draw();
        for (auto& sprite : sprites) {
            sprite->draw();
        }
        spotlight.draw();
    }
    hud.draw();
    mainMenu.draw();
    viewport.draw();
    SDL_RenderPresent(renderer);
}

void Engine::update(Uint32 ticks) {
    if (!paused) {
        for (auto& sprite : sprites) {
            sprite->update(ticks);
        }
        checkForCollisions();
        if (zombieManager.waveIsDead(sprites) &&
            (wavesDefeated++ < zombieManager.getAmountOfWaves())) {
            zombieManager.purgeContainerOfZombies(sprites);
            zombieManager.getNextWave(sprites);
            bulletManager.reclaimBullets(sprites);
        }
        spotlight.update(ticks);
        back.update();
        bulletManager.update(ticks, sprites);
    }
    if (player->hasFinishedExploding() && !inMenu) {
        inMenu = true;
        paused = true;
        mainMenu.setTitleText("Play again?");
        mainMenu.setSubtitleText("press r to replay or q to quit");
        mainMenu.showMenu();
    } else if (zombieManager.getAmountOfWaves() == wavesDefeated) {
        inMenu = true;
        paused = true;
        mainMenu.setTitleText("You win! Play again?");
        mainMenu.setSubtitleText("press r to replay or q to quit");
        mainMenu.showMenu();
    }
    viewport.update();  // always update viewport last
}

void Engine::checkForCollisions() {
    for (unsigned int i = 0; i < sprites.size(); i++) {
        for (unsigned int j = i + 1; j < sprites.size(); j++) {
            if (!sprites[i]->hasFinishedExploding() &&
                !sprites[j]->hasFinishedExploding()) {
                collisionChecker.resolve(sprites[i], sprites[j]);
            }
        }
    }
}

void Engine::switchSprite() {
    ++currentSprite;
    Viewport::getInstance().setObjectToTrack(
        sprites[currentSprite % sprites.size()]);
}

void Engine::play() {
    SDL_Event event;
    const Uint8* keystate;
    bool done = false;
    Uint32 ticks = clock.getElapsedTicks();
    FrameGenerator frameGen;

    while (!done) {
        // The next loop polls for events, guarding against key bounce:
        while (SDL_PollEvent(&event)) {
            keystate = SDL_GetKeyboardState(NULL);
            if (event.type == SDL_QUIT) {
                done = true;
                break;
            }
            if (event.type == SDL_KEYDOWN) {
                if (keystate[SDL_SCANCODE_ESCAPE] || keystate[SDL_SCANCODE_Q]) {
                    done = true;
                    break;
                }
                if (keystate[SDL_SCANCODE_P] && !inMenu) {
                    paused = !paused;
                }
                if (keystate[SDL_SCANCODE_F1]) {
                    hud.toggleVisibility();
                }
                if (keystate[SDL_SCANCODE_E]) {
                    sprites[currentSprite]->explode();
                }
                if (keystate[SDL_SCANCODE_G]) {
                    player->toggleGodMode();
                }
                if (keystate[SDL_SCANCODE_SPACE]) {
                    if (inMenu) {
                        mainMenu.hideMenu();
                        inMenu = false;
                        paused = false;
                        hud.toggleVisibility();
                    } else {
                        bulletManager.produceBulletFrom(player, sprites);
                        sdlSound[0];
                    }
                }
                if (keystate[SDL_SCANCODE_R]) {
                    if (inMenu &&
                        (player->hasFinishedExploding() ||
                         zombieManager.getAmountOfWaves() == wavesDefeated)) {
                        zombieManager.resetToFirstWave(sprites);
                        inMenu = false;
                        paused = false;
                        mainMenu.hideMenu();
                        wavesDefeated = 0;
                        player->reset();
                        bulletManager.reclaimBullets(sprites);
                    }
                }
                if (keystate[SDL_SCANCODE_M]) {
                    sdlSound.toggleMusic();
                }
            }
        }

        // In this section of the event loop we allow key bounce:

        ticks = clock.getElapsedTicks();
        if (ticks > 0) {
            clock.incrFrame();
            if (keystate[SDL_SCANCODE_A]) {
                player->left();
            }
            if (keystate[SDL_SCANCODE_D]) {
                player->right();
            }
            if (keystate[SDL_SCANCODE_W]) {
                player->up();
            }
            if (keystate[SDL_SCANCODE_S]) {
                player->down();
            }
            draw();
            update(ticks);
            if (makeVideo) {
                frameGen.makeFrame();
            }
        }
    }
}
