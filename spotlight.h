#ifndef SPOTLIGHT__H
#define SPOTLIGHT__H
#include "SDL.h"
#include "sprite.h"
#include <vector>

class Spotlight : public Sprite{
  public:
    static Spotlight& getInstance();
    Spotlight(const Spotlight&) = delete;
    Spotlight& operator=(const Spotlight&) = delete;
    void update(Uint32 ticks);

  private:
    Spotlight();
};
#endif
