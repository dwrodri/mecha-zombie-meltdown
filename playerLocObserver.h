#ifndef PLAYER_LOC_OBSERVER__H
#define PLAYER_LOC_OBSERVER__H
// This class observes the player, and performs calcs for the NPCs to chase the
// player
class PlayerLocObserver {
  public:
    static PlayerLocObserver& getInstance() {
        static PlayerLocObserver instance;
        return instance;
    }
    void notify(Vector2f msg) { player_location = msg; }
    Vector2f getPlayerLoc() const { return player_location; }

  private:
    PlayerLocObserver() : player_location(Vector2f(0, 0)) {}
    Vector2f player_location;
};
#endif
