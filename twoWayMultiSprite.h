#ifndef TWO_WAY_MULTISPRITE__H
#define TWO_WAY_MULTISPRITE__H
#include <string>
#include <vector>
#include "multisprite.h"

class TwoWayMultiSprite : public MultiSprite {
  public:
    TwoWayMultiSprite(const std::string&);
    TwoWayMultiSprite(const TwoWayMultiSprite&) = delete;
    TwoWayMultiSprite& operator=(const TwoWayMultiSprite&) = delete;

    void draw() const;
    void update(Uint32 ticks);

  private:
    SDL_RendererFlip flip;
};

#endif
