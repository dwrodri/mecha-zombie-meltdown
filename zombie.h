#ifndef ZOMBIE__H
#define ZOMBIE__H
#include <vector>
#include "multisprite.h"
#include "playerLocObserver.h"

class Zombie : public MultiSprite {
  public:
    Zombie();
    Zombie(const Zombie&) = delete;
    Zombie& operator=(const Zombie&) = delete;

    void broadcast();
    void update(Uint32 ticks);
    void draw() const;
    void randomSpawn();
    void reset();
    void wound();
    int getHealth() const;
    bool isDead();

  private:
    void advanceFrame(Uint32 ticks);
    bool collision;
    Vector2f movingVelocity;
    int framesPerState;
    int stateOffset;
    int health;
};
#endif
