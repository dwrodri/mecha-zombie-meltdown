#ifndef MAIN_MENU__H
#define MAIN_MENU__H
#include <SDL.h>
#include <string>

class MainMenu {
  public:
    static MainMenu& getInstance();
    MainMenu(const MainMenu&) = delete;
    MainMenu& operator=(const MainMenu&) = delete;

    void draw() const;
    void toggleVisibility() { visible = !visible; }
    void showMenu() { visible = true; }
    void hideMenu() { visible = false; }
    void setTitleText(const std::string new_title);
    void setSubtitleText(const std::string new_subtitle);

  private:
    MainMenu();
    unsigned int worldWidth;
    unsigned int worldHeight;
    bool visible;
    std::string title;
    std::string subtitle;
    SDL_Color titleColor;
    SDL_Color subtitleColor;
    unsigned int titleSize;
    unsigned int subtitleSize;
};
#endif
