#include "multisprite.h"
#include "player.h"
class BulletManager {
  public:
    static BulletManager& getInstance();
    ~BulletManager();
    BulletManager(const BulletManager&) = delete;
    BulletManager& operator=(const BulletManager&) = delete;

    int getRemainingBullets();
    unsigned int getClipSize() const;
    void produceBulletFrom(Player* shooter,
                           std::vector<MultiSprite*>& main_sprite_pool);
    void reclaimBullets(std::vector<MultiSprite*>& main_sprite_pool);
    void update(Uint32 ticks, std::vector<MultiSprite*>& main_sprite_pool);
    void recoverBullet(std::vector<MultiSprite*>& main_sprite_pool);

  private:
    BulletManager();
    unsigned int bulletPoolSize;
    int bulletSpeed;
    unsigned int regenCooldown;
    unsigned int timeSinceLastRegen;
    std::vector<MultiSprite*> bulletPool;
    std::vector<MultiSprite*>::iterator nextUnusedBulletIter;
};
