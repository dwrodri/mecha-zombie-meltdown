#define PURE 0

class Observer {
  public:
    virtual Observer(arg) = PURE;
    Observer& operator=(const Observer& rhs);
    virtual ~Observer();

  private:
    char* MEMBER;
};
