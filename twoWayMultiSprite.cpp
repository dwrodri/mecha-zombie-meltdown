#include "twoWayMultiSprite.h"
#include "gameData.h"
#include "imageFactory.h"

TwoWayMultiSprite::TwoWayMultiSprite(const std::string& name)
    : MultiSprite(name), flip(SDL_FLIP_NONE) {}

void TwoWayMultiSprite::draw() const {
    images[currentFrame]->draw(getX(), getY(), getScale(), flip);
}

void TwoWayMultiSprite::update(Uint32 ticks) {
    MultiSprite::update(ticks);
    flip = getVelocityX() < 0 ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE;
}
