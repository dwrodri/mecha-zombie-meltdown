CXX =g++

# Warnings frequently signal eventual errors:
CXXFLAGS=`sdl2-config --cflags` -g -W -Wall -std=c++14 -Weffc++ -Wextra -Wpedantic -O0 -I `sdl2-config --prefix`/include/

LDFLAGS = `sdl2-config --libs` -lm -lexpat -lSDL2_ttf -lSDL2_image -lSDL2_mixer

OBJS = \
  explodingSprite.o \
  chunk.o \
  renderContext.o \
	ioMod.o \
	parseXML.o \
	gameData.o \
    hud.o \
    mainMenu.o \
	viewport.o \
	world.o \
    spotlight.o \
	spriteSheet.o \
	image.o \
	imageFactory.o \
	frameGenerator.o \
	sprite.o \
	multisprite.o \
	bulletManager.o \
	zombie.o \
	player.o \
	vector2f.o \
	clock.o \
	engine.o \
	sound.o \
	main.o
EXEC = run

%.o: %.cpp %.h
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(EXEC): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJS) $(LDFLAGS)

clean:
	rm -rf $(OBJS)
	rm -rf $(EXEC)
	rm -rf frames/*.bmp
