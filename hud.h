#ifndef HUD__H
#define HUD__H
#include "gameData.h"

class Hud {
  public:
    static Hud& getInstance();
    Hud(const Hud&) = delete;
    Hud& operator=(const Hud&) = delete;

    void draw() const;
    void toggleVisibility() { visible = !visible; }

  private:
    Hud();
    const Gamedata& gdata;
    unsigned int worldWidth;
    unsigned int worldHeight;
    Vector2f position;
    static bool visible;
};
#endif
