#include "multisprite.h"
#include "gameData.h"
#include "imageFactory.h"

void MultiSprite::advanceFrame(Uint32 ticks) {
    timeSinceLastFrame += ticks;
    if (timeSinceLastFrame > frameInterval) {
        currentFrame = (currentFrame + 1) % numberOfFrames;
        timeSinceLastFrame = 0;
    }
}

MultiSprite::MultiSprite(const std::string& name)
    : Drawable(
          name,
          Vector2f(Gamedata::getInstance().getXmlInt(name + "/startLoc/x"),
                   Gamedata::getInstance().getXmlInt(name + "/startLoc/y")),
          Vector2f(Gamedata::getInstance().getXmlInt(name + "/speedX"),
                   Gamedata::getInstance().getXmlInt(name + "/speedY")),
          Gamedata::getInstance().getXmlFloat(name + "/scale")),
      images(ImageFactory::getInstance().getImages(name)),
      explosion(nullptr),

      begunExploding(false),
      stoppedExploding(false),
      currentFrame(0),
      numberOfFrames(Gamedata::getInstance().getXmlInt(name + "/frames")),
      frameInterval(Gamedata::getInstance().getXmlInt(name + "/frameInterval")),
      timeSinceLastFrame(0),
      worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
      worldHeight(Gamedata::getInstance().getXmlInt("world/height")) {}

void MultiSprite::draw() const {
    if (explosion) {
        explosion->draw();
    } else if (!begunExploding) {
        images[currentFrame]->draw(getX(), getY(), getScale());
    }
}

void MultiSprite::explode() {
    if (!explosion && !stoppedExploding) {
        Sprite sprite(getName(), getPosition(), getVelocity(),
                      images[currentFrame]);
        sprite.setScale(getScale());
        explosion = new ExplodingSprite(sprite);
        begunExploding = true;
    }
}

void MultiSprite::update(Uint32 ticks) {
    if (explosion) {
        explosion->update(ticks);
        if (explosion->chunkCount() == 0) {
            delete explosion;
            explosion = nullptr;
            stoppedExploding = true;
        }
        return;
    }
    advanceFrame(ticks);

    Vector2f incr = getVelocity() * static_cast<double>(ticks) * 0.001;
    setPosition(getPosition() + incr);
}
