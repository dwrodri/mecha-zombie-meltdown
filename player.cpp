#include "player.h"
#include <algorithm>
#include "SDL.h"
#include "gameData.h"
#include "renderContext.h"
Player::Player(const std::string& name)
    : MultiSprite(name),
      movingVelocity(
          Gamedata::getInstance().getXmlInt(name + "/movingVelocity"),
          Gamedata::getInstance().getXmlInt(name + "/movingVelocity")),
      framesPerState(Gamedata::getInstance().getXmlInt(name + "/perState")),
      orientation(static_cast<unsigned>(Orientation::DOWN)),
      health(1 << 16),
      godMode(false),
      observer(nullptr) {}

void Player::advanceFrame(Uint32 ticks) {
    timeSinceLastFrame += ticks;
    if (timeSinceLastFrame > frameInterval) {
        currentFrame = (orientation * framesPerState) +
                       ((currentFrame + 1) % framesPerState);
        timeSinceLastFrame = 0;
    }
}

void Player::stop() {
    setVelocityX(0.93 * getVelocityX());
    setVelocityY(0.93 * getVelocityY());
}

void Player::up() {
    if (getY() > 0) {
        setVelocityY(-movingVelocity[1]);
    }
    orientation = static_cast<unsigned>(Orientation::UP);
}

void Player::down() {
    if (getY() < worldHeight - getScaledHeight()) {
        setVelocityY(movingVelocity[1]);
    }
    orientation = static_cast<unsigned>(Orientation::DOWN);
}

void Player::left() {
    if (getX() > 0) {
        setVelocityX(-movingVelocity[0]);
    }
    orientation = static_cast<unsigned>(Orientation::LEFT);
}

void Player::right() {
    if (getX() < worldWidth - getScaledWidth()) {
        setVelocityX(movingVelocity[0]);
    }
    orientation = static_cast<unsigned>(Orientation::RIGHT);
}

unsigned int Player::getOrientation() const { return orientation; }

void Player::update(Uint32 ticks) {
    MultiSprite::update(ticks);
    if (observer) {
        broadcast();
    }
    if (health <= 0) {
        explode();
    }
    stop();
}

void Player::wound() {
    if (!godMode) {
        health -= 64;
    }
}

void Player::reset() {
    health = 1 << 16;
    unexplode();
    setPosition(
        Vector2f(Gamedata::getInstance().getXmlInt("world/width") / 2,
                 Gamedata::getInstance().getXmlInt("world/height") / 2));
}
void Player::attach(PlayerLocObserver* obs) { observer = obs; }

void Player::detach() { observer = nullptr; }

void Player::broadcast() { observer->notify(getPosition()); }

void Player::draw() const {
    MultiSprite::draw();
    static SDL_Rect health_bar_red, health_bar_green;
    health_bar_red.x = health_bar_green.x = getPosition()[0];
    health_bar_red.y = health_bar_green.y = getPosition()[1] - 8;
    health_bar_red.w = getScaledWidth();
    health_bar_green.w = (int)((health / 65536.0) * (health_bar_red.w));
    health_bar_red.h = health_bar_green.h = 4;
    SDL_SetRenderDrawColor(RenderContext::getInstance().getRenderer(), 255, 0,
                           0, 255);
    SDL_RenderFillRect(RenderContext::getInstance().getRenderer(),
                       &health_bar_red);
    SDL_SetRenderDrawColor(RenderContext::getInstance().getRenderer(), 0, 255,
                           0, 255);
    SDL_RenderFillRect(RenderContext::getInstance().getRenderer(),
                       &health_bar_green);
}

void Player::toggleGodMode() { godMode = !godMode; }
