#include "spotlight.h"
#include "gameData.h"
#include "playerLocObserver.h"
#include "renderContext.h"

Spotlight& Spotlight::getInstance() {
    static Spotlight instance;
    return instance;
}

Spotlight::Spotlight(): Sprite("spotlight") {}

void Spotlight::update(Uint32 ticks) {
    Sprite::update(ticks);
    Vector2f currentPlayerLoc = PlayerLocObserver::getInstance().getPlayerLoc();
    setPosition(currentPlayerLoc - Vector2f(1024 - 64, 1024 - 64)); 
}
