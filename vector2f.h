#ifndef VECTOR2F__H
#define VECTOR2F__H

#include <iostream>

class Vector2f {
  public:
    Vector2f(double x = 0, double y = 0);

    double &operator[](int index);
    double operator[](int index) const;

    bool operator==(const Vector2f &other) const {
        return (v[0] == other.v[0] && v[1] == other.v[1]);
    }

    bool operator!=(const Vector2f &other) const {
        return (v[0] != other.v[0] or v[1] != other.v[1]);
    }

    Vector2f operator*(double scale) const;
    Vector2f operator/(double scale) const;

    Vector2f operator+(const Vector2f &other) const;
    Vector2f operator-(const Vector2f &other) const;
    Vector2f operator-() const;

    const Vector2f &operator*=(double scale);
    const Vector2f &operator/=(double scale);
    const Vector2f &operator+=(const Vector2f &other);
    const Vector2f &operator-=(const Vector2f &other);

    double magnitude() const;
    double magnitudeSquared() const;
    Vector2f normalize() const;
    double dot(const Vector2f &other) const;
    Vector2f cross(const Vector2f &other) const;
    Vector2f &operator=(const Vector2f &);
    Vector2f abs() const;

  private:
    double v[2];
};

Vector2f operator*(double scale, const Vector2f &v);
std::ostream &operator<<(std::ostream &output, const Vector2f &v);

#endif
