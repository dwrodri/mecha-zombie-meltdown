#include <cmath>
#include <iostream>

#include "chunk.h"

void Chunk::update(Uint32 ticks) {
    double yincr = getVelocityY() * static_cast<double>(ticks) * 0.001;
    setY(getY() - yincr);
    double xincr = getVelocityX() * static_cast<double>(ticks) * 0.001;
    setX(getX() - xincr);
    distance += (hypot(xincr, yincr));
    if (distance > maxDistance) tooFar = true;
}
