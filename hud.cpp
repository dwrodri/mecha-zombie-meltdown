#include "hud.h"
#include <sstream>
#include "bulletManager.h"
#include "clock.h"
#include "ioMod.h"
#include "renderContext.h"

bool Hud::visible = true;

Hud& Hud::getInstance() {
    static Hud hud;
    return hud;
}

Hud::Hud()
    : gdata(Gamedata::getInstance()),
      worldWidth(gdata.getXmlInt("world/width")),
      worldHeight(gdata.getXmlInt("world/height")),
      position(Vector2f(gdata.getXmlInt("hud/loc/x"),
                        gdata.getXmlInt("hud/loc/y"))) {}

void Hud::draw() const {
    if (visible) {
        SDL_SetRenderDrawColor(RenderContext::getInstance().getRenderer(), 81,
                               82, 135, 192);  // set color for the background
        SDL_Rect background;
        background.x = static_cast<Uint8>(position[0]);
        background.y = static_cast<Uint8>(position[1]);
        background.w = static_cast<Uint8>(worldWidth / 5.5);
        background.h = static_cast<Uint8>(worldHeight / 4.5);
        SDL_RenderFillRect(RenderContext::getInstance().getRenderer(),
                           &background);
        // draw fps
        std::stringstream fps_strm;
        fps_strm << Clock::getInstance().getFps() << std::flush;
        IoMod::getInstance().writeText("FPS: " + fps_strm.str(),
                                       static_cast<int>(position[0]) + 10,
                                       static_cast<int>(position[1]) + 5);
        // draw instructions
        IoMod::getInstance().writeText("WASD to move",
                                       static_cast<int>(position[0]) + 10,
                                       static_cast<int>(position[1]) + 25);
        IoMod::getInstance().writeText("F1 to toggle HUD",
                                       static_cast<int>(position[0]) + 10,
                                       static_cast<int>(position[1]) + 45);
        IoMod::getInstance().writeText("Space to Shoot",
                                       static_cast<int>(position[0]) + 10,
                                       static_cast<int>(position[1]) + 65);
        IoMod::getInstance().writeText("R to Gather Energy",
                                       static_cast<int>(position[0]) + 10,
                                       static_cast<int>(position[1]) + 85);
        IoMod::getInstance().writeText("G for God mode",
                                       static_cast<int>(position[0]) + 10,
                                       static_cast<int>(position[1]) + 105);

        // draw bullet pool stats
        std::stringstream bullet_pool_strm;
        bullet_pool_strm << BulletManager::getInstance().getRemainingBullets()
                         << "/" << BulletManager::getInstance().getClipSize()
                         << std::flush;
        IoMod::getInstance().writeText("Energy: " + bullet_pool_strm.str(),
                                       static_cast<int>(position[0]) + 10,
                                       static_cast<int>(position[1]) + 125);
    }
}
