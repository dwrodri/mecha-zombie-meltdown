#ifndef GAMEDATA__H
#define GAMEDATA__H
#include <map>
#include <random>
#include <string>
#include "parseXML.h"
#include "vector2f.h"

class Gamedata {
  public:
    static Gamedata& getInstance();
    Gamedata(const Gamedata&) = delete;
    Gamedata& operator=(const Gamedata&) = delete;
    ~Gamedata();
    void displayData() const;

    bool getXmlBool(const std::string&) const;
    const std::string& getXmlStr(const std::string&) const;
    double getXmlFloat(const std::string&) const;
    int getXmlInt(const std::string&) const;
    double getRandInRange(int min, int max) const;
    double getRandFloat(double min, double max) const;
    bool checkTag(const std::string&) const;

  private:
    ParseXML parser;
    const map<std::string, std::string> gameData;
    Gamedata(const std::string& fn = "xmlSpec/game.xml");
};
#endif
