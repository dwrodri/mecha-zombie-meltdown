#include <SDL.h>
#include <utility>
#include <vector>
#include "bulletManager.h"
#include "clock.h"
#include "collisionChecker.h"
#include "hud.h"
#include "ioMod.h"
#include "mainMenu.h"
#include "player.h"
#include "playerLocObserver.h"
#include "renderContext.h"
#include "sound.h"
#include "spotlight.h"
#include "viewport.h"
#include "world.h"
#include "zombieManager.h"

class Engine {
  public:
    Engine();
    ~Engine();
    void play();
    void switchSprite();

  private:
    const RenderContext& rc;
    const IoMod& io;
    Clock& clock;
    CollisionChecker& collisionChecker;
    PlayerLocObserver& playerLocObserver;
    ZombieManager& zombieManager;
    BulletManager& bulletManager;
    SDLSound& sdlSound;
    Spotlight& spotlight;

    SDL_Renderer* const renderer;
    World back;
    Hud& hud;
    MainMenu& mainMenu;
    Viewport& viewport;

    std::vector<MultiSprite*> sprites;
    Player* player;
    int currentSprite;
    unsigned int wavesDefeated;

    bool makeVideo;
    bool inMenu;
    bool paused;

    void draw() const;
    void update(Uint32);

    Engine(const Engine&);
    Engine& operator=(const Engine&);
    void printScales() const;
    void checkForCollisions();
};
