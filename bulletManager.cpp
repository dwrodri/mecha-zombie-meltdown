#include "bulletManager.h"
#include <algorithm>
#include <cmath>
#include <memory>
#include "gameData.h"
#include "sound.h"

BulletManager& BulletManager::getInstance() {
    static BulletManager instance;
    return instance;
}

BulletManager::~BulletManager() {
    for (auto& bullet : bulletPool) {
        delete bullet;
    }
}

BulletManager::BulletManager()
    : bulletPoolSize(Gamedata::getInstance().getXmlInt("bullet_pool_size")),
      bulletSpeed(Gamedata::getInstance().getXmlInt("bullet_speed")),
      regenCooldown(Gamedata::getInstance().getXmlInt("regen_cooldown")),
      timeSinceLastRegen(0),
      bulletPool(),
      nextUnusedBulletIter() {
    bulletPool.reserve(bulletPoolSize);
    for (unsigned int i = 0; i < bulletPoolSize; i++) {
        bulletPool.emplace_back(new MultiSprite("bullet"));
    }
    nextUnusedBulletIter = bulletPool.begin();
}

int BulletManager::getRemainingBullets() {
    return std::distance(nextUnusedBulletIter, bulletPool.end());
}

unsigned int BulletManager::getClipSize() const { return bulletPoolSize; }

void BulletManager::produceBulletFrom(
    Player* shooter, std::vector<MultiSprite*>& main_sprite_pool) {
    if (nextUnusedBulletIter == bulletPool.end()) return;
    MultiSprite* nextBullet = *(nextUnusedBulletIter++);
    double horiz_offset = shooter->getScaledWidth() + 5;
    double vert_offset = shooter->getScaledHeight() + 5;
    switch (shooter->getOrientation()) {
        case 0:  // up
            vert_offset *= -1;
            horiz_offset = 0;
            nextBullet->setVelocityX(0);
            nextBullet->setVelocityY(bulletSpeed * -1);
            break;
        case 1:  // down
            horiz_offset = 0;
            nextBullet->setVelocityX(0);
            nextBullet->setVelocityY(bulletSpeed);
            break;
        case 2:  // left
            vert_offset = 0;
            horiz_offset *= -1;
            nextBullet->setVelocityY(0);
            nextBullet->setVelocityX(bulletSpeed * -1);
            break;
        case 3:  // right
            vert_offset = 0;
            nextBullet->setVelocityY(0);
            nextBullet->setVelocityX(bulletSpeed);
            break;
        default:
            std::cout << "BAD" << std::endl;
            break;
    }
    nextBullet->setPosition(shooter->getPosition() +
                            Vector2f(horiz_offset, vert_offset));
    // std::cout << "Bullet addr:\t" << std::addressof(*nextBullet) <<
    // std::endl; std::cout << "Bullet pos:\t" << nextBullet->getPosition() <<
    // std::endl; std::cout << "Bullet exploded:\t";
    if (nextBullet->hasFinishedExploding()) {
        nextBullet->unexplode();
    }
    // (nextBullet->hasFinishedExploding()) ? std::cout << "Yes" <<std::endl :
    // std::cout << "No" << std::endl;
    main_sprite_pool.emplace_back(nextBullet);
}

void BulletManager::recoverBullet(std::vector<MultiSprite*>& main_sprite_pool) {
    auto pool_iter = main_sprite_pool.begin();
    while (pool_iter != main_sprite_pool.end()) {
        bool offScreen =
            ((*pool_iter)->getX() < 0 || (*pool_iter)->getY() < 0) ||
            ((*pool_iter)->getPosition().magnitudeSquared() >
             Vector2f(1024, 1024).magnitudeSquared());
        if ((*pool_iter)->getName() == "bullet" &&
            ((*pool_iter)->hasFinishedExploding() || offScreen)) {
            (*pool_iter)->unexplode();
            main_sprite_pool.erase(pool_iter);
            // std::cout << std::addressof(**pool_iter) << std::endl;
            if (nextUnusedBulletIter != bulletPool.begin())
                nextUnusedBulletIter--;
            break;
        } else {
            pool_iter++;
        }
    }
}

void BulletManager::update(Uint32 ticks,
                           std::vector<MultiSprite*>& main_sprite_pool) {
    timeSinceLastRegen += ticks;
    if (timeSinceLastRegen > regenCooldown) {
        recoverBullet(main_sprite_pool);
        timeSinceLastRegen = 0;
    }
}

void BulletManager::reclaimBullets(
    std::vector<MultiSprite*>& main_sprite_pool) {
    auto pool_iter = main_sprite_pool.begin();
    while (pool_iter != main_sprite_pool.end()) {
        if ((*pool_iter)->getName() == std::string("bullet") &&
            (*pool_iter)->hasFinishedExploding()) {
            main_sprite_pool.erase(pool_iter);
        } else {
            pool_iter++;
        }
    }
    nextUnusedBulletIter = bulletPool.begin();
    std::for_each(bulletPool.begin(), bulletPool.end(),
                  [](auto& x) { x->unexplode(); });
}
