#ifndef PLAYER__H
#define PLAYER__H
#include <vector>
#include "multisprite.h"
#include "playerLocObserver.h"

// a variant of multisprite with
// explosions, projectile pool,
// and all the other important features
class HyperSprite : public MultiSprite {
  public:
    Player(const std::string&);
    Player(const Player&) = delete;
    Player& operator=(const Player&) = delete;
    void update(Uint32 ticks);
    void right();
    void left();
    void up();
    void down();
    void stop();
    void draw() const;
    template <class T>
    void attach(PlayerLocObserver* obs);
    void detach();
    void broadcast();

  private:
    void advanceFrame(Uint32 ticks);
    Vector2f movingVelocity;
    int framesPerState;
    int stateOffset;
    template <class T>
    std::vector<T> observerList;
};
#endif
