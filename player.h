#ifndef PLAYER__H
#define PLAYER__H
#include <vector>
#include "multisprite.h"
#include "playerLocObserver.h"

enum class Orientation { UP, DOWN, LEFT, RIGHT };

class Player : public MultiSprite {
  public:
    Player(const std::string&);
    Player(const Player&) = delete;
    Player& operator=(const Player&) = delete;
    void update(Uint32 ticks);
    void right();
    void left();
    void up();
    void down();
    void stop();
    void wound();
    void reset();
    void draw() const;
    void toggleGodMode();
    void attach(PlayerLocObserver* obs);
    void detach();
    void broadcast();
    unsigned getOrientation() const;

  private:
    void advanceFrame(Uint32 ticks);
    Vector2f movingVelocity;
    unsigned int framesPerState;
    unsigned int orientation;
    int health;
    bool godMode;
    PlayerLocObserver* observer;
};
#endif
