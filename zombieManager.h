#include <algorithm>
#include "gameData.h"
#include "multisprite.h"
#include "playerLocObserver.h"
#include "zombie.h"

class ZombieManager {
  public:
    static ZombieManager& getInstance() {
        static ZombieManager instance;
        return instance;
    }
    ~ZombieManager() {
        for (auto& zombie : zombie_pool) {
            delete zombie;
        }
    }
    ZombieManager(const ZombieManager&) = delete;
    ZombieManager& operator=(const ZombieManager&) = delete;

    void getNextWave(std::vector<MultiSprite*>& main_sprite_pool) {
        // reset all zombies
        for (auto& zombie : zombie_pool) {
            dynamic_cast<Zombie*>(zombie)->reset();
            dynamic_cast<Zombie*>(zombie)->randomSpawn();
        }
        // now put double prevoius amount in
        // main_sprite_pool
        std::cout << "loading " << getAmountOfZombiesInWave()
                  << "zombies from pool" << std::endl;
        for (unsigned int i = 0; i < getAmountOfZombiesInWave(); i++) {
            main_sprite_pool.emplace_back(zombie_pool[i]);
        }
        current_wave_count++;
    }

    void purgeContainerOfZombies(std::vector<MultiSprite*>& main_sprite_pool) {
        for (auto iter = main_sprite_pool.begin();
             iter != main_sprite_pool.end();
             iter = (dynamic_cast<Zombie*>(*iter))
                        ? main_sprite_pool.erase(iter)
                        : iter + 1)
            ;
    }

    void resetToFirstWave(std::vector<MultiSprite*>& main_sprite_pool) {
        purgeContainerOfZombies(main_sprite_pool);
        for (auto& zombie : zombie_pool) {
            dynamic_cast<Zombie*>(zombie)->reset();
            dynamic_cast<Zombie*>(zombie)->randomSpawn();
        }
        current_wave_count = 0;
        getNextWave(main_sprite_pool);
    }

    bool waveIsDead(std::vector<MultiSprite*>& main_sprite_pool) {
        if (std::all_of(
                main_sprite_pool.begin(), main_sprite_pool.end(),
                [](auto& sprite) { return sprite->getName() != "zombie"; })) {
            return false;
        }

        for (auto& sprite : main_sprite_pool) {
            if (sprite->getName() == "zombie" &&
                !sprite->hasFinishedExploding()) {
                return false;
            }
        }

        std::cout << "wave is dead" << std::endl;
        return true;
    }

    unsigned int getAmountOfZombiesInWave() const {
        return starting_zombie_count << current_wave_count;
    }

    unsigned int getAmountOfWaves() const { return wave_count; }

  private:
    ZombieManager()
        : starting_zombie_count(
              Gamedata::getInstance().getXmlInt("start_count")),
          wave_count(Gamedata::getInstance().getXmlInt("waves")),
          current_wave_count(0),
          zombie_pool() {
        std::cout << "making pool with "
                  << (starting_zombie_count << wave_count) << "zombies"
                  << std::endl;
        zombie_pool.reserve(starting_zombie_count << wave_count);
        for (size_t i = 0; i < zombie_pool.capacity(); i++) {
            zombie_pool.emplace_back(new Zombie());
            dynamic_cast<Zombie*>(zombie_pool.back())->randomSpawn();
        }
    }

    const unsigned int starting_zombie_count;
    const unsigned int wave_count;
    unsigned int current_wave_count;
    std::vector<MultiSprite*> zombie_pool;
};
