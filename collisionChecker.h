#ifndef COLLISION_CHECKER__H
#define COLLISION_CHECKER__H
#include <algorithm>
#include <array>
#include <cmath>
#include "multisprite.h"
#include "player.h"
#include "zombie.h"

class CollisionChecker {
  public:
    static CollisionChecker& getInstance() {
        static CollisionChecker c;
        return c;
    }
    CollisionChecker(const CollisionChecker&) = delete;
    CollisionChecker& operator=(const CollisionChecker&) = delete;

    // NOTE: If one of Multisprites is the player, then it is
    // DEFINITELY the first one, and the bool is used to tell
    // this class
    void resolve(MultiSprite* character, MultiSprite* otherCharacter) {
        Vector2f characterCenter =
            Vector2f(character->getX() + character->getScaledWidth() * 0.5,
                     character->getY() + character->getScaledHeight() * 0.5);
        Vector2f otherCharacterCenter = Vector2f(
            otherCharacter->getX() + otherCharacter->getScaledWidth() * 0.5,
            otherCharacter->getY() + otherCharacter->getScaledHeight() * 0.5);
        int biggest_dim =
            std::max({character->getScaledWidth(), character->getScaledHeight(),
                      otherCharacter->getScaledWidth(),
                      otherCharacter->getScaledHeight()});
        bool haveCollided =
            (characterCenter - otherCharacterCenter).magnitude() <= biggest_dim;
        if (haveCollided) {
            if (character->getName() == "bullet" &&
                otherCharacter->getName() == "zombie") {
                dynamic_cast<Zombie*>(otherCharacter)->wound();
                character->explode();
            } else if (otherCharacter->getName() == std::string("bullet") &&
                       character->getName() == "zombie") {
                dynamic_cast<Zombie*>(character)->wound();
                otherCharacter->explode();
            }
            if (character->getName() == "player" &&
                otherCharacter->getName() == "zombie") {
                dynamic_cast<Player*>(character)->wound();
            } else if (otherCharacter->getName() == "player" &&
                       character->getName() == "zombie") {
                dynamic_cast<Player*>(otherCharacter)->wound();
            }
            Vector2f bounce_vec =
                (otherCharacterCenter - characterCenter).normalize();
            Vector2f collision_midpoint = {
                (characterCenter[0] + otherCharacterCenter[0]) / 2,
                (characterCenter[1] + otherCharacterCenter[1]) / 2};
            character->setPosition(
                (collision_midpoint - (bounce_vec * (biggest_dim / 2))) -
                Vector2f(character->getScaledWidth() * 0.5,
                         character->getScaledHeight() * 0.5));
            otherCharacter->setPosition(
                (collision_midpoint + (bounce_vec * (biggest_dim / 2))) -
                Vector2f(otherCharacter->getScaledWidth() * 0.5,
                         otherCharacter->getScaledHeight() * 0.5));
        }
    }

  private:
    CollisionChecker() = default;
};

#endif
