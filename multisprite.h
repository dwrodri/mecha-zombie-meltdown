#ifndef MULTISPRITE__H
#define MULTISPRITE__H
#include <cmath>
#include <string>
#include <vector>
#include "drawable.h"
#include "explodingSprite.h"

class MultiSprite : public Drawable {
  public:
    MultiSprite(const std::string&);
    MultiSprite(const MultiSprite&) = delete;
    MultiSprite& operator=(const MultiSprite&) = delete;

    void draw() const;
    virtual void update(Uint32 ticks);

    const Image* getImage() const { return images[currentFrame]; }
    int getScaledWidth() const {
        return getScale() * images[currentFrame]->getWidth();
    }
    int getScaledHeight() const {
        return getScale() * images[currentFrame]->getHeight();
    }
    virtual const SDL_Surface* getSurface() const {
        return images[currentFrame]->getSurface();
    }

    virtual void explode();
    bool hasFinishedExploding() { return stoppedExploding; }
    void unexplode() { begunExploding = stoppedExploding = false; }

  protected:
    std::vector<Image*> images;
    ExplodingSprite* explosion;

    bool begunExploding;
    bool stoppedExploding;
    unsigned currentFrame;
    unsigned numberOfFrames;
    unsigned frameInterval;
    double timeSinceLastFrame;
    int worldWidth;
    int worldHeight;

    virtual void advanceFrame(Uint32 ticks);
};
#endif
