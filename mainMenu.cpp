#include "mainMenu.h"
#include "gameData.h"
#include "ioMod.h"
#include "renderContext.h"

MainMenu& MainMenu::getInstance() {
    static MainMenu instance;
    return instance;
}

void MainMenu::draw() const {
    if (visible) {
        // set screen to translucent black
        SDL_SetRenderDrawColor(RenderContext::getInstance().getRenderer(), 32,
                               32, 32, 24);
        SDL_Rect background;
        background.x = 0;
        background.y = 0;
        background.w = worldWidth;
        background.h = worldHeight;
        SDL_RenderFillRect(RenderContext::getInstance().getRenderer(),
                           &background);
        // draw title
        IoMod::getInstance().writeText(title, 20, worldHeight / 4, titleColor,
                                       titleSize);
        // draw subTitle
        IoMod::getInstance().writeText(subtitle, 20,
                                       titleSize * 2 + worldHeight / 4,
                                       subtitleColor, subtitleSize);
    }
}

void MainMenu::setTitleText(const std::string new_title) { title = new_title; }

void MainMenu::setSubtitleText(const std::string new_subtitle) {
    subtitle = new_subtitle;
}
MainMenu::MainMenu()
    : worldWidth(Gamedata::getInstance().getXmlInt("world/width")),
      worldHeight(Gamedata::getInstance().getXmlInt("world/height")),
      visible(true),
      title("Title Text"),
      subtitle("Subtitle Text"),
      titleColor({static_cast<Uint8>(
                      Gamedata::getInstance().getXmlInt("menutitle/red")),
                  static_cast<Uint8>(
                      Gamedata::getInstance().getXmlInt("menutitle/blue")),
                  static_cast<Uint8>(
                      Gamedata::getInstance().getXmlInt("menutitle/green")),
                  static_cast<Uint8>(
                      Gamedata::getInstance().getXmlInt("menutitle/alpha"))}),
      subtitleColor({static_cast<Uint8>(
                         Gamedata::getInstance().getXmlInt("menusubtitle/red")),
                     static_cast<Uint8>(Gamedata::getInstance().getXmlInt(
                         "menusubtitle/blue")),
                     static_cast<Uint8>(Gamedata::getInstance().getXmlInt(
                         "menusubtitle/green")),
                     static_cast<Uint8>(Gamedata::getInstance().getXmlInt(
                         "menusubtitle/alpha"))}),
      titleSize(Gamedata::getInstance().getXmlInt("menutitle/size")),
      subtitleSize(Gamedata::getInstance().getXmlInt("menusubtitle/size")) {}
