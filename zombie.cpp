#include "zombie.h"
#include <cmath>
#include "gameData.h"
Zombie::Zombie()
    : MultiSprite("zombie"),
      collision(false),
      movingVelocity(
          Gamedata::getInstance().getXmlInt("zombie/movingVelocity"),
          Gamedata::getInstance().getXmlInt("zombie/movingVelocity")),
      framesPerState(Gamedata::getInstance().getXmlInt("zombie/perState")),
      stateOffset(0),
      health(1 << 8) {}

void Zombie::advanceFrame(Uint32 ticks) {
    timeSinceLastFrame += ticks;
    if (timeSinceLastFrame > frameInterval) {
        currentFrame = (stateOffset * framesPerState) +
                       ((currentFrame + 1) % framesPerState);
        timeSinceLastFrame = 0;
    }
}

void Zombie::update(Uint32 ticks) {
    setVelocity(
        (PlayerLocObserver::getInstance().getPlayerLoc() - getPosition())
            .normalize() *
        movingVelocity[0]);
    if (std::abs(getVelocityX()) >
        std::abs(getVelocityY())) {  // find principal component of vector
        stateOffset =
            (getVelocityX() > 0) ? 3 : 2;  // pick between left or right
    } else {
        stateOffset = (getVelocityY() > 0) ? 1 : 0;  // pick between up or down
    }
    if (!collision) {
        MultiSprite::update(ticks);
    }
    if (health <= 0) {
        explode();
    }
}

void Zombie::draw() const { MultiSprite::draw(); }

void Zombie::randomSpawn() {
    setPosition(Vector2f(Gamedata::getInstance().getRandInRange(0, 1024),
                         Gamedata::getInstance().getRandInRange(0, 1024)));
}

void Zombie::reset() {
    health = 1 << 8;
    randomSpawn();
    unexplode();
}

void Zombie::wound() {
    health -= 32;
    if (health == 0) {
        explode();
    }
}

int Zombie::getHealth() const { return health; }

bool Zombie::isDead() { return (health > 0); }
